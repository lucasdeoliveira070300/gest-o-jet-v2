module.exports = {
    dialect: 'postgres',
    host: 'localhost',
    port: '5432',
    username: 'postgres',
    password: '12kaspire12',
    database: 'gestao_jet',
    define: {
        timestamps: true,
        underscored: true,
        underscoredAll: true,
    }
}
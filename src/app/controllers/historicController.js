// o Yup nao tem um export default, desta forma precisamos importar tudo com *
import * as Yup from 'yup';
import User from '../models/User';
import Event from '../models/Event';
import Status from '../models/type_status';
import Historic from '../models/Historic';

class HistoricController {
  async store(req, res) {
    try {
      // aqui estou validando o objeto, pois o req.body é um objeto
      // e estou passando o formato que eu quero que ele tenha
      // neste formato passo todas propriedades de cada atributo do model
      const schema = Yup.object().shape({
        id_event: Yup.number().required(),
        id_user: Yup.number().required(),
        id_status: Yup.number().required(),   
        checkin: Yup.bool().required(),
      });

      // aqui verifico se o schema anterior é valido, passando os dados do req.body
      if (!(await schema.isValid(req.body))) {
        return res.status(400).json({ error: 'Validation fails!' });
      }

      const userExists = await User.findOne({
        attributes: ['id_user', 'username'],
        where: {
          id_user: req.body.id_user,
        },
      });

      if (!(userExists)) {
        return res.status(400).json({ error: 'User does not exist yet' });
      }

      const EventExists = await Event.findOne({
        attributes: ['id_event'],
        where: {
          id_event: req.body.id_event,
        },
      });

      if (!(EventExists)) {
        return res.status(400).json({ error: 'This event does not exist' });
      }

      const StatusExists = await Status.findOne({
        attributes: ['id_type_status'],
        where: {
          id_type_status: req.body.id_status,
        },
      });

      if (!(StatusExists)) {
        return res.status(400).json({ error: 'This status does not exist' });
      }

      console.log(req.body);
      const { id_historic ,id_event, id_user, id_status, checkin } = await Historic.create(req.body);
      return res.json({
        id_historic,
        id_event, 
        id_user,
        id_status,
        checkin
      });
    } catch (err) {
        return res.status(400).json({
            message: `An error occurred (${err.message}). Please contact your system administrator`,
        });
    }
  }
  async index(req,res){
    const historic = await Historic.findAll({
      order: ['id_historic'],
      attributes: ['id_historic','id_event', 'id_user', 'id_status', 'checkin']
    });
    return res.json(historic);
  }
}

export default new HistoricController();

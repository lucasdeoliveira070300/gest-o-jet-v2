// o Yup nao tem um export default, desta forma precisamos importar tudo com *
import * as Yup from 'yup';
import User from '../models/User';
import EventType from '../models/Type_evt_pub';
import Event from '../models/Event';
import Moment from 'moment';

class EventController {
  async store(req, res) {
    try {
      // aqui estou validando o objeto, pois o req.body é um objeto
      // e estou passando o formato que eu quero que ele tenha
      // neste formato passo todas propriedades de cada atributo do model
      const schema = Yup.object().shape({
        id_user: Yup.number().required(),
        id_type: Yup.number().required(),
        subject: Yup.string().required().max(100),
        description: Yup.string().required().max(255),
        date: Yup.string().required().max(10),
        hour:Yup.string().required().max(5),
        local: Yup.string().required().max(100) 
      });

      // aqui verifico se o schema anterior é valido, passando os dados do req.body
      if (!(await schema.isValid(req.body))) {
        return res.status(400).json({ error: 'Validation fails!' });
      }

      const userExists = await User.findOne({
        attributes: ['id_user', 'username'],
        where: {
          username: req.body.username,
        },
      });

      if (!(userExists)) {
        return res.status(400).json({ error: 'User does not exist yet' });
      }

      const TypeExists = await EventType.findOne({
        attributes: ['id_evt_pub', 'description'],
        where: {
          id_evt_pub: req.body.id_type,
        },
      });

      if (!(TypeExists)) {
        return res.status(400).json({ error: 'This type does not exist' });
      }

      const DiaAtual = Moment().format("DD/MM/YYYY");
      
      if(req.body.date < DiaAtual){
        return res.status(400).json({ error: 'The event date cannot be less than the current date' });
      }

      console.log(req.body);
      const { id_event, id_user, id_type, subject, description, date, hour, local } = await Event.create(req.body);
      return res.json({
        id_event, 
        id_user,
        id_type,
        subject,
        description,
        date,
        hour,
        local
      });
    } catch (err) {
        return res.status(400).json({
            message: `An error occurred (${err.message}). Please contact your system administrator`,
        });
    }
  }
  async index(req,res){
    const event = await Event.findAll({
      order: ['date'],
      attributes: ['id_event', 'subject', 'description', 'date', 'hour']
    });
    return res.json(event);
  }
}

export default new EventController();

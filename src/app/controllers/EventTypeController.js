// o Yup nao tem um export default, desta forma precisamos importar tudo com *
import * as Yup from 'yup';
import EventType from '../models/Type_evt_pub';

class EventTypeController {
  // store pode ser feito por um user sem estar logado
  // que vai efetuar se cadastro no sistema
  async store(req, res) {
    try {
      // aqui estou validando o objeto, pois o req.body é um objeto
      // e estou passando o formato que eu quero que ele tenha
      // neste formato passo todas propriedades de cada atributo do model
      const schema = Yup.object().shape({
        description: Yup.string().required().max(50),
      });

      // aqui verifico se o schema anterior é valido, passando os dados do req.body
      if (!(await schema.isValid(req.body))) {
        return res.status(400).json({ error: 'Validation fails!' });
      }

      const { id_evt_pub, description } = await EventType.create(req.body);
      return res.json({
        id_evt_pub,
        description
      });
    } catch (err) {
      return res.status(400).json({
        message: `An error occurred (${err.message}). Please contact your system administrator`,
      });
    }
  }
}

export default new EventTypeController();

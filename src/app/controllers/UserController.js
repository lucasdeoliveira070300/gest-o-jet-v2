// o Yup nao tem um export default, desta forma precisamos importar tudo com *
import * as Yup from 'yup';
import User from '../models/User';

class UserController {
  // store pode ser feito por um user sem estar logado
  // que vai efetuar se cadastro no sistema
  async store(req, res) {
    try {
      // aqui estou validando o objeto, pois o req.body é um objeto
      // e estou passando o formato que eu quero que ele tenha
      // neste formato passo todas propriedades de cada atributo do model
      const schema = Yup.object().shape({
        id_user_type: Yup.number().required(),
        name: Yup.string().required().max(50),
        date_birth: Yup.date().required(),
        email: Yup.string().required().max(50),
        telefone: Yup.string().required().max(30),
        username: Yup.string().required().max(20),
        password_virtual: Yup.string().required().min(6).max(12),
      });

      // aqui verifico se o schema anterior é valido, passando os dados do req.body
      if (!(await schema.isValid(req.body))) {
        return res.status(400).json({ error: 'Validation fails!' });
      }

      const userExists = await User.findOne({
        attributes: ['id_user', 'email'],
        where: {
          email: req.body.email,
        },
      });

      if (userExists) {
        return res.status(400).json({ error: 'User already exists!' });
      }
      console.log(req.body);
      const { id_user, name, email } = await User.create(req.body);
      return res.json({
        id_user,
        name,
        email,
      });
    } catch (err) {
      return res.status(400).json({
        message: `An error occurred (${err.message}). Please contact your system administrator`,
      });
    }
  }
  
  async index(req,res){
    const users = await User.findAll({
      order: ['name'],
      attributes: ['id_user', 'name', 'email', 'telefone','date_birth']
    });
    return res.json(users);
  }
}

export default new UserController();

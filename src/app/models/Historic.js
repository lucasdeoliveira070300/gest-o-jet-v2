import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Historic extends Model {

    static init(sequelize){
        super.init(
        {
            id_historic: { 
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            id_event: Sequelize.INTEGER,
            id_user: Sequelize.INTEGER,
            id_status: Sequelize.INTEGER,
            checkin: Sequelize.BOOLEAN,
            },
            {
            sequelize,
            freezeTableName: 'Historic',
            tableName: 'Historic'
            }
        );
        return this;
    }

    static associate(models){
        this.belongsTo(models.Event, {foreignKey: 'id_event', as: 'event'});
        this.belongsTo(models.User, {foreignKey: 'id_user', as: 'user'});
        this.belongsTo(models.Type_status, {foreignKey: 'id_type_status', as: 'type_status'});
    }
}
export default Historic;
import Sequelize, { Model } from 'sequelize';

class User_type extends Model {

    static init(sequelize){
        super.init(
        {
            id_user_type: { 
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            description: Sequelize.STRING,
            },
            {
            sequelize,
            freezeTableName: 'User_type',
            tableName: 'User_type'
            }
        );
        return this;
    }
}
export default User_type;
import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Type_evt_pub extends Model {

    static init(sequelize){
        super.init(
        {
            id_evt_pub: { 
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            description: Sequelize.STRING,
            },
            {
            sequelize,
            freezeTableName: 'Type_evt_pub',
            tableName: 'Type_evt_pub'
            }
        );
        return this;
    }
}
export default Type_evt_pub;
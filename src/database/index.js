import Sequelize    from 'sequelize';
import User         from '../app/models/User';
import Event        from '../app/models/Event';
import Historic     from '../app/models/Historic';
import User_type    from '../app/models/User_type';
import Type_evt_pub from '../app/models/Type_evt_pub';
import type_status  from '../app/models/type_status';
import Dbconfig     from '../config/database';

const models = [User_type, User, Event, Historic, Type_evt_pub, type_status];

class Database{
    constructor(){
        this.init();
    }
    init(){
        this.connection = new Sequelize(Dbconfig);
        models
        .map(model => model.init(this.connection))
        .map(model => model.associate && model.associate(this.connection.models));
    }
}

export default new Database;
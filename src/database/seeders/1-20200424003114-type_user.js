module.exports = {
  up: QueryInterface => {
    return QueryInterface.bulkInsert('User_type', [
      {
        description: 'Admin',
        created_at: new Date(),
        updated_at: new Date()
      },
      {         
        description: 'Teacher',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        description: 'Students',
        created_at: new Date(),
        updated_at: new Date()
      }
    ],
    {}
    );
  },

  down: () => {
  
  }
};

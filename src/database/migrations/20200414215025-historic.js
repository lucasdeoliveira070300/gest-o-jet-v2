module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Historic',{
      id_historic:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      id_event:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        references: {model: 'Event', key: 'id_event'},
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      id_user:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        references: {model: 'User', key: 'id_user'},
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      id_status:{
        type: Sequelize.INTEGER, 
        allowNull: false,
        references: {model: 'Type_status', key: 'id_type_status'},
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      checkin:{
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      created_at:{
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at:{
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Historic');
  }
};
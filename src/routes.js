import { Router } from 'express';
import SessionController from './app/controllers/SessionController';
import UserController from './app/controllers/Usercontroller';
import UserTypeController from './app/controllers/UserTypeController';
import authMiddleware from './app/middlewares/auth';
import adminMiddleware from './app/middlewares/admin';
import EventController from './app/controllers/EventController'
import EventTypeController from './app/controllers/EventTypeController';
import historicController from './app/controllers/historicController';
import StatusTypeController from './app/controllers/StatusTypeController';

const routes = new Router();

routes.post('/User', UserController.store);
routes.post('/Session', SessionController.session);

routes.use(authMiddleware);

routes.get('/User',adminMiddleware, UserController.index);
routes.post('/UserType',UserTypeController.store);

routes.post('/Event_type', EventTypeController.store);

routes.post('/Event', EventController.store);
routes.get('/Event', EventController.index);

routes.post('/StatusType', StatusTypeController.store);

routes.post('/Historic', historicController.store);
routes.get('/Historic',adminMiddleware, historicController.index);

export default routes;
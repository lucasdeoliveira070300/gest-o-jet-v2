import express from 'express';
import routes from './routes';
import Youch from 'youch';
import 'express-async-errors';
import './database';

class Index{
    constructor(){
        this.app = express();
        this.middlewares();
        this.routes();
        this.exceptionHandler();
    }

    middlewares(){
        this.app.use(express.json());
    }

    routes(){
        this.app.use(routes);
    }
    exceptionHandler(){
        this.app.use(async(err, req, res, next)=>{
            const errors = await new Youch(err, req).toJSON();
            return res.status(500).json(errors);
        });
    }
}

export default new Index().app;